package kz.sdu.edu.sdutechnopark.admin_panel.links;

public interface MainLinkService {
    void update(String name, String updateLink);
}
